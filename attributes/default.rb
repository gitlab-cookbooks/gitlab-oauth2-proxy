default['gitlab-oauth2-proxy']['log_root'] = '/var/log/oauth2_proxy'
default['gitlab-oauth2-proxy']['upstream'] = 'http://127.0.0.1:8080/'
default['gitlab-oauth2-proxy']['redirect_url'] = ''
default['gitlab-oauth2-proxy']['svlogd_extra_opts'] = ''
default['gitlab-oauth2-proxy']['client_id'] = 'set in chef vault'
default['gitlab-oauth2-proxy']['client_secret'] = 'set in chef vault'
default['gitlab-oauth2-proxy']['provider'] = 'gitlab'
default['gitlab-oauth2-proxy']['login_url'] = 'https://dev.gitlab.org/oauth/authorize'
default['gitlab-oauth2-proxy']['redeem_url'] = 'https://dev.gitlab.org/oauth/token'
default['gitlab-oauth2-proxy']['validate_url'] = 'https://dev.gitlab.org/api/v3/user'
default['gitlab-oauth2-proxy']['cookie_name'] = '_oauth2_proxy'
default['gitlab-oauth2-proxy']['cookie_secret'] = 'set in chef vault'

default['gitlab-oauth2-proxy']['nginx']['enable'] = false
default['gitlab-oauth2-proxy']['nginx']['fqdn'] = node['fqdn']
default['gitlab-oauth2-proxy']['nginx']['default_server'] = false
default['gitlab-oauth2-proxy']['nginx']['ssl_certificate'] = 'set in chef vault'
default['gitlab-oauth2-proxy']['nginx']['ssl_key'] = 'set in chef vault'
