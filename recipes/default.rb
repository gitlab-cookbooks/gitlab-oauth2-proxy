#
# Cookbook:: gitlab-oauth2-proxy
# Recipe:: default
#
# Copyright:: 2016, GitLab Inc.
#
# License: MIT
#
include_recipe 'gitlab-vault'

# Fetch secrets from Chef Vault
oauth2_proxy_conf = GitLab::Vault.get(node, 'gitlab-oauth2-proxy')

# Install oauth2_proxy
version = 'oauth2_proxy-v3.2.0.linux-amd64.go1.11'
download = "https://github.com/oauth2-proxy/oauth2-proxy/releases/download/v3.2.0/#{version}.tar.gz"
download_dir = "/opt/oauth2_proxy/#{version}"

tar_extract download do
  source download
  target_dir download_dir
  creates "#{download_dir}/oauth2_proxy-linux-amd64"
  tar_flags [ '--strip-components 1' ]
end

link 'oauth2_proxy' do
  target_file '/usr/local/sbin/oauth2_proxy'
  to "#{download_dir}/oauth2_proxy-linux-amd64"
  notifies :run, 'execute[sv restart oauth2_proxy]', :delayed
end

# Setup oauth2_proxy service
package 'runit'

sv_dir = '/etc/sv/oauth2_proxy'
sv_log_dir = File.join(sv_dir, 'log')

directory sv_dir do
  mode '0700'
  recursive true
end

directory sv_log_dir do
  mode '0700'
  recursive true
end

directory '/var/log/oauth2_proxy' do
  mode '0700'
  owner 'www-data'
  recursive true
end

template File.join(sv_dir, 'run') do
  source 'oauth2_proxy-run.erb'
  mode '0755'
  notifies :run, 'execute[sv restart oauth2_proxy]'
end

template File.join(sv_log_dir, 'run') do
  source 'oauth2_proxy-log-run.erb'
  mode '0755'
  variables(oauth2_proxy_conf.to_hash)
end

template '/etc/oauth2_proxy.cfg' do
  mode '0600'
  user 'www-data'
  group 'www-data'
  variables(oauth2_proxy_conf.to_hash)
end

link '/etc/service/oauth2_proxy' do
  to sv_dir
end

execute 'sv restart oauth2_proxy' do
  action :nothing
end

# Install and setup nginx with ssl
if oauth2_proxy_conf['nginx']['enable']
  file "/etc/ssl/#{oauth2_proxy_conf['nginx']['fqdn']}.crt" do
    content oauth2_proxy_conf['nginx']['ssl_certificate']
    owner 'root'
    group 'root'
    mode '644'
  end

  file "/etc/ssl/#{oauth2_proxy_conf['nginx']['fqdn']}.key" do
    content oauth2_proxy_conf['nginx']['ssl_key']
    owner 'root'
    group 'root'
    mode '600'
  end

  template "#{node['nginx']['dir']}/sites-available/oauth2-proxy" do
    source 'nginx-oauth2-proxy.erb'
    owner  'root'
    group  'root'
    mode   '0644'
    variables(
      fqdn: oauth2_proxy_conf['nginx']['fqdn'],
      default_server: oauth2_proxy_conf['nginx']['default_server']
    )
  end

  include_recipe 'chef_nginx'

  nginx_site 'default' do
    enable false
  end

  nginx_site 'oauth2-proxy' do
    enable true
    notifies :restart, 'service[nginx]', :delayed
  end
end
