# frozen_string_literal: true

# Cookbook:: gitlab-oauth2-proxy
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab-oauth2-proxy::default' do
  context 'When the default recipe is run' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node, _server|
      end.converge(described_recipe)
    end
    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
    it 'creates oauth config with restricted domains' do
      expect(chef_run).to create_template('/etc/oauth2_proxy.cfg')
      expect(chef_run).to(render_file('/etc/oauth2_proxy.cfg').with_content do |c|
        allowed_domains = <<~ALLOWED
          email_domains = [
            "gitlab.com"
          ]
        ALLOWED
        expect(c).to include(allowed_domains)
      end)
    end
  end
  context 'When svlogd options are set' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node, _server|
        node.normal['gitlab-oauth2-proxy']['svlogd_extra_opts'] = '-n100'
      end.converge(described_recipe)
    end
    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
    it 'creates config with svlogd extra options' do
      expect(chef_run).to create_template('/etc/sv/oauth2_proxy/log/run')
      expect(chef_run).to(render_file('/etc/sv/oauth2_proxy/log/run').with_content do |c|
        expect(c).to include('exec svlogd -tt -n100 /var/log/oauth2_proxy')
      end)
    end
  end
end
